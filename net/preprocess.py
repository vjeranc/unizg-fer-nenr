import pickle as pi
import numpy as np

letters = ['alpha', 'beta', 'gamma', 'delta', 'eta']

def load_letters(ls=letters):
    zips_l = []
    for letter in ls:
        with open(letter+'.bin', 'rb') as fi:
            ll = pi.load(fi)
        zips_l.extend(list(map(lambda x: (x, letter), ll)))

    return zips_l

def normalize_raw(xs):
    x = np.array(xs, dtype=np.float32)
    orderd = x.view('f4,f4').argsort(order=['f1'], axis=0)
    orderd = orderd.reshape((np.prod(orderd.shape),))


    mean_x = np.mean(x, axis=0)
    x -= mean_x

    max_x = np.max(np.abs(x))
    x /= max_x

    return x

def create_feature_vector(x, M=20):
    pass

def create_label_vector(y):
    global letters
    return np.array(map(lambda x: y==x, letters),dtype=np.float32)

def save_learning_set(X, Y, file_name='learning_set.bin'):
    with open(file_name, 'wb') as fi:
        pi.dump((X, Y), fi)
