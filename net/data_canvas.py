from tkinter import *
import pickle as pi

canvas_width = 500
canvas_height = 500

x, y = [None, None]
w = None

points_list = []

if len(sys.argv) < 2:
    exit("Gimme file name.")
try:
    with open(sys.argv[1], 'rb') as fi:
        s = fi.read()
        if len(s) > 0:
            points_list = pi.loads(s)
except:
    print("Creating file.")

master = Tk()

def paint( event ):
    global x, y, w
    python_green = "#476042"
    if not x:
        w.delete("all")
        x, y = event.x, event.y
        points_list.append([(x, y)])
    else:
        w.create_line( x, y, event.x, event.y, fill = python_green )
        x, y = event.x, event.y
        points_list[-1].append((x, y))

def reset( event ):
    global x, y
    x = None; y = None

def on_closing():
    global points_list
    with open(sys.argv[1],'wb') as fi:
        pi.dump(points_list, fi)

    master.destroy()


master.title( "Painting using Ovals" )
w = Canvas(master,
           width=canvas_width,
           height=canvas_height-10)
w.pack(expand = YES, fill = BOTH)
w.bind( "<B1-Motion>", paint )
w.bind( "<ButtonRelease-1>", reset )

message = Label( master, text = "Press and Drag the mouse to draw" )
message.pack( side = BOTTOM )

master.protocol("WM_DELETE_WINDOW", on_closing)


mainloop()
