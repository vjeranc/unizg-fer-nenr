import numpy as np
import pickle as pi
import itertools as it
import sklearn.utils as utils
import copy

def create_dummy_learning_set(fi):
    X = np.linspace(-1, 1, 10)
    X = X.reshape((X.shape[0], 1))
    Y = X * X
    pi.dump((X, Y),fi)

class Network(object):

    def __init__(self, sizes):
        self.num_layers = len(sizes)
        self.sizes = sizes
        self.biases = [np.random.randn(1, y).astype('f2') for y in sizes[1:]]
        self.weights = [np.random.randn(x, y).astype('f2')
                        for x, y in zip(sizes[:-1], sizes[1:])]

    def update_mini_batch(self, mini_batch, eta):
        """Update the network's weights and biases by applying
        gradient descent using backpropagation to a single mini batch.
        The "mini_batch" is a list of tuples "(x, y)", and "eta"
        is the learning rate."""
        n_b = [np.zeros(b.shape).astype('f2') for b in self.biases]
        n_w = [np.zeros(w.shape).astype('f2') for w in self.weights]

        x, y = mini_batch

        delta_n_b, delta_n_w = self.backprop(x, y)
        for b, w, db, dw in zip(n_b, n_w, delta_n_b, delta_n_w):
            b += db.sum(0)
            w += dw.sum(0)

        for b, w, nb, nw in zip(self.biases, self.weights, n_b, n_w):
            b -= (eta/len(mini_batch))*nb
            w -= (eta/len(mini_batch))*nw

    def _forward(self, x):
        # feedforward
        activation = x
        activations = [x] # list to store all the activations, layer by layer
        zs = [] # list to store all the z vectors, layer by layer

        for b, w in zip(self.biases, self.weights):
            z = activation.dot(w)+b
            zs.append(sigmoid(z))
            activation = zs[-1]
            activations.append(activation)

        return zs, activations

    def forward(self, x):
        x = np.append(x, np.ones((x.shape[0],1)), axis=1).astype('f2')
        return self._forward(x)[1][-1]

    def backprop(self, x, y):
        """Return a tuple "(n_b, n_w)" representing the
        gradient for the cost function C_x.  "n_b" and
        "n_w" are layer-by-layer lists of numpy arrays, similar
        to "self.biases" and "self.weights"."""
        n_b = [np.zeros(b.shape) for b in self.biases]
        n_w = [np.zeros(w.shape) for w in self.weights]
        # feedforward
        zs, activations = self._forward(x)
        # backward pass
        delta = (activations[-1] - y) * (zs[-1] * (1 - zs[-1]))
        n_b[-1] = delta
        n_w[-1] = activations[-2].T.dot(delta)

        for l in range(2, self.num_layers):
            sp = zs[-l] * (1 - zs[-l])
            delta = delta.dot(self.weights[-l+1].T) * sp
            n_b[-l] = delta
            n_w[-l] = activations[-l-1].T.dot(delta)

        return (n_b, n_w)


def cost_derivative(self, output_activations, y):
    """Return the vector of partial derivatives \partial C_x /
    \partial a for the output activations."""
    return (output_activations-y)

def sigmoid(z):
    """The sigmoid function."""
    return 1.0/(1.0+np.exp(-z))

def train_neural_net(X, Y, dimensions, min_err, eta, method='bp', maxiter=50000, nn=None):
    X = np.append(X, np.ones((X.shape[0],1)), axis=1).astype('f2')
    dims = copy.copy(dimensions)
    dims.insert(0, X.shape[1])
    dims.append(Y.shape[1])
    nn = Network(dims) if nn is None else nn
    iternum = 0
    if method=='bp':
        while iternum < maxiter:
            nn.update_mini_batch((X, Y), eta)
            Y_ = nn._forward(X)[1][-1]
            diff = Y_ - Y
            err = diff.T.dot(diff)
            if iternum % 10000 == 0:
                print(err[0,0])
            if err < min_err:
                break
            iternum += 1

    elif method=='sbp':
        idx = np.arange(len(X))
        while iternum < maxiter:
            i = np.random.choice(idx)
            x, y = X[i], Y[i]
            x = x.reshape((1,)+x.shape)
            y = y.reshape((1,)+y.shape)
            nn.update_mini_batch((x, y), eta)
            if iternum % 10000 == 0:
                Y_ = nn._forward(X)[1][-1]
                diff = Y_ - Y
                err = diff.T.dot(diff)
                print(err[0,0])
            if err < min_err:
                break
            iternum += 1
    elif method=='mbbp':
        pass
    else:
        raise ValueError('Wrong method, available bp, sbp and mbbp.')

    return nn

def error(nn, X, Y):
    Y_ = nn.forward(X)
    diff = Y_ - Y
    return diff.T.dot(diff)[0,0]

if __name__ == '__main__':
    if len(sys.argv) == 2:
        with open(sys.argv[1], 'wb') as fi:
            create_dummy_learning_set(fi)
        exit()

    arch = map(int, sys.argv[1].split('x'))
    X, Y = pi.load(sys.argv[2])
