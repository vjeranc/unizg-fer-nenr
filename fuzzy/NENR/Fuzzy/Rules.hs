{-# LANGUAGE RecordWildCards #-}
module NENR.Fuzzy.Rules (
 FuzzyRule
,createFuzzyRule
,inferFromRule
,inferFromRules)
where


import NENR.Fuzzy.Set
import NENR.IntRange
import NENR.Fuzzy.Operations
import NENR.Fuzzy.Defuzzy
import qualified Data.Map as M
import Data.Maybe (fromMaybe)
import Control.Arrow
-- import Debug.Trace

data FuzzyRule a = FR {
  conj :: M.Map a FuzzySet
 ,concl :: FuzzySet
}

createFuzzyRule :: Ord a => [(a, FuzzySet)] -> FuzzySet -> FuzzyRule a
createFuzzyRule xs c = FR {conj=M.fromList xs, concl=c}

inferFromRule :: Ord a => [(a, DomainElement)] -> FuzzyRule a -> FuzzySet
inferFromRule xs FR{..} = applyUnary concl (zadehAnd mn)
  where mn  = minimum fss
        fss = map (fromMaybe 1E100 . uncurry lkup . first (`M.lookup`conj)) xs
        lkup Nothing _ = Nothing
        lkup (Just fs) y = Just $ get fs y

inferFromRules :: Ord a => [(a, DomainElement)] -> [FuzzyRule a] -> DomainElement
inferFromRules xs rs = centerOfArea union
  where infers = map (inferFromRule xs) rs
        union = foldl1 (\fsx fsy -> applyBinary fsx fsy zadehOr) infers
