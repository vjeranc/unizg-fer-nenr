module NENR.Fuzzy.Defuzzy (centerOfArea) where


import NENR.Fuzzy.Set
import NENR.IntRange

import Control.Arrow
-- import Debug.Trace

centerOfArea :: FuzzySet -> DomainElement
centerOfArea fs = domainElement res
    where res = map round $ divL val ss
          d = domain fs
          vals = map (get fs) $ toList d
          val = foldr1 add $ map (uncurry mul . second toDouble) . zip vals $ toList d
          ss = sum vals
          mul x = map (x*)
          add = zipWith (+)
          divL xs x = zipWith (/) xs (if x/=0.0 then repeat x else repeat 1.0)
