module NENR.Fuzzy.Relations (
isUTimesURelation,
isSymmetric,
isReflexive,
isMaxMinTransitive,
isFuzzyEquivalence,
compositionOfBinaryRelations
) where

import NENR.Fuzzy.Set
import NENR.IntRange

import Control.Arrow
import Data.List (transpose)

isUTimesURelation :: FuzzySet -> Bool
isUTimesURelation fs = n == 2 && l
  where d = domain fs
        n = dimension d
        l = isUTimesUDomain d

isFuzzyEquivalence :: FuzzySet -> Bool
isFuzzyEquivalence fs =
  and $ [isReflexive, isSymmetric, isMaxMinTransitive] <*> [fs]

isSymmetric :: FuzzySet -> Bool
isSymmetric fs = n == 2 && symmetryTest
  where d = domain fs
        n = dimension d
        g = get fs
        symmetryTest = all (\e -> g e == g (reverseDE e)) $ toList d

isReflexive :: FuzzySet -> Bool
isReflexive fs = n == 2 && reflexivityTest
  where d = domain fs
        n = dimension d
        g = get fs
        reflexivityTest = all ((==1).g) . filter allEq $ toList d

isMaxMinTransitive :: FuzzySet -> Bool
isMaxMinTransitive fs = n == 2 && transitivityTest
  where d = domain fs
        n = dimension d
        g = get fs
        l = map (g &&& id) $ toList d
        s = transList d
        transitivityTest = all elCheck l
        elCheck :: (Double, DomainElement) -> Bool
        elCheck (r, el) = r >= maximum (map (uncurry min . (g *** g)) (s el))

compositionOfBinaryRelations :: FuzzySet -> FuzzySet -> FuzzySet
compositionOfBinaryRelations f1 f2
  | not (n1 == 2 && n2 == 2) = error "not binary relations"
  | otherwise = f3
  where d1 = domain f1
        d2 = domain f2
        n1 = dimension d1
        n2 = dimension d2
        d3 = merge d1 d2
        g1 = get f1
        g2 = get f2
        l1 = map (map g1) $ matrixForm d1
        l2 = map (map g2) $ matrixForm d2
        f3 = constructFuzzySet d3 . zip (toList d3) . concat $ mmult l1 l2

mmult :: (Num a, Ord a) => [[a]] -> [[a]] -> [[a]]
mmult a b = [ [ maximum $ zipWith min ar bc | bc <- transpose b ] | ar <- a ]
