{-# LANGUAGE RecordWildCards #-}
module NENR.Fuzzy.Set (
FuzzySet(get, domain),
constructEmptyFuzzySet,
constructFuzzySet,
constructCalculatedFuzzySet,
constructLambdaSet,
constructLSet,
constructGammaSet,
set,
lambdaFunction,
gammaFunction,
lFunction,
prettyPrint
)
where

import NENR.IntRange hiding (prettyPrint)
import Data.Array.Unboxed
import Control.Arrow (first)
import Control.Monad (forM_)
import Text.Printf

data FuzzySet = FS {
  f :: UArray Int Double
, domain :: IntRange
, get :: DomainElement -> Double
}

instance Show FuzzySet where
  show FS{..} = show f

-- | Fuzzy set is constructed with its member function being zero for the whole
-- domain.
constructEmptyFuzzySet :: IntRange -> FuzzySet
constructEmptyFuzzySet domain =
  constructFuzzySetGeneral domain $ zip [0..] [0.0,0.0..]

constructCalculatedFuzzySet :: IntRange -> (Double -> Double) -> FuzzySet
constructCalculatedFuzzySet domain f =
  constructFuzzySetGeneral domain . zip [0..] $ map f [0..]

constructFuzzySet :: IntRange -> [(DomainElement, Double)] -> FuzzySet
constructFuzzySet domain vals =
  constructFuzzySetGeneral domain . map (first $ indexOf domain) $ vals

constructFuzzySetGeneral :: IntRange -> [(Int, Double)] -> FuzzySet
constructFuzzySetGeneral domain vals =
  let n = cardinality domain
      name = FS { f      = array (0,n-1) $ take n vals,
                  domain = domain,
                  get    = defaultGet name}
  in name

set :: FuzzySet -> [(DomainElement, Double)] -> FuzzySet
set fs@FS{..} xs = fs {f = f // idxVals}
  where idxVals = map (first $ indexOf domain) xs

defaultGet :: FuzzySet -> DomainElement -> Double
defaultGet FS{..} x = f ! indexOf domain x

lambdaFunction :: (Fractional a, Ord a) => a -> a -> a -> a -> a
lambdaFunction a b c x
  | a <= x && x <= b = f a b
  | b <= x && x <= c = 1.0 - f b c
  | otherwise = 0.0
    where f x0 x1 = (x - x0) * 1.0 / (x1 - x0)

gammaFunction :: (Fractional a, Ord a) => a -> a -> a -> a
gammaFunction a b x
  | x <= a    = 0.0
  | x <= b    = (x-a) / (b-a)
  | otherwise = 1.0

lFunction :: (Fractional a, Ord a) => a -> a -> a -> a
lFunction a b x
  | x <= a    = 1.0
  | x <= b    = 1.0 - (x-a) / (b-a)
  | otherwise = 0.0

prettyPrint :: FuzzySet -> String -> IO ()
prettyPrint FS{..} s = do
  putStrLn s
  forM_ (zip [0..] $ toList domain) $
    \(i, x) -> printf "d%s = %.6f\n" (show x) (f ! i)

constructLambdaSet d a b c =
  constructCalculatedFuzzySet d $
    lambdaFunction
      (fromIntegral . indexOf d $ domainElement [a])
      (fromIntegral . indexOf d $ domainElement [b])
      (fromIntegral . indexOf d $ domainElement [c])

constructGammaSet d a b =
  constructCalculatedFuzzySet d $
    gammaFunction
      (fromIntegral . indexOf d $ domainElement [a])
      (fromIntegral . indexOf d $ domainElement [b])

constructLSet d a b =
  constructCalculatedFuzzySet d $
    lFunction
      (fromIntegral . indexOf d $ domainElement [a])
      (fromIntegral . indexOf d $ domainElement [b])
