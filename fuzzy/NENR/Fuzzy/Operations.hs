module NENR.Fuzzy.Operations (
 zadehNot
,zadehAnd
,zadehOr
,hamacherTNorm
,hamacherSNorm
,applyUnary
,applyBinary
,algebraicProduct
) where

import NENR.Fuzzy.Set (FuzzySet(..), constructFuzzySet)
import NENR.IntRange (toList)
import Control.Arrow

zadehNot :: (Num a) => a -> a
zadehNot = (1-)

zadehAnd :: Ord a => a -> a -> a
zadehAnd x y = min x y

zadehOr :: Ord a => a -> a -> a
zadehOr x y = max x y

hamacherTNorm :: Fractional a => a -> a -> a -> a
hamacherTNorm p x y = x*y / (p + (1-p) * (x + y - x*y))

hamacherSNorm :: Fractional a => a -> a -> a -> a
hamacherSNorm p x y = (x+y-(2-p) * x * y) / (1 - (1-p) * x * y)

algebraicProduct :: Fractional a => a -> a -> a
algebraicProduct = (*)

applyUnary :: FuzzySet -> (Double -> Double) -> FuzzySet
applyUnary fs f =
  let d = domain fs
      g = get fs
  in constructFuzzySet d $ map (id &&& (f . g)) (toList d)

applyBinary :: FuzzySet -> FuzzySet -> (Double -> Double -> Double) -> FuzzySet
applyBinary fs1 fs2 f =
  let d = domain fs1
      g1 = get fs1
      g2 = get fs2
      els = toList d
  in constructFuzzySet d $ map (id &&& (uncurry f . (g1 &&& g2))) els
