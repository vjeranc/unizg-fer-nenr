module NENR.IntRange
(
IntRange,
DomainElement,
reverseDE,
allEq,
getComponent,
toDouble,
constructIntRange,
toList,
cardinality,
dimension,
numberOfComponents,
domainElement,
prettyPrint,
combine,
elementForIndex,
indexOf,
transList,
isUTimesUDomain,
matrixForm,
merge
)
where

import Control.Monad (forM_)
import Data.List (nub)
import Text.Printf

newtype DomainElement = DE [Int] deriving (Eq, Ord)

reverseDE :: DomainElement -> DomainElement
reverseDE (DE xs) = DE $ reverse xs

allEq :: DomainElement -> Bool
allEq (DE (x:xs)) = all (==x) xs

getComponent :: Int -> DomainElement -> Int
getComponent n (DE xs) = xs !! n

toDouble :: DomainElement -> [Double]
toDouble (DE xs) = map fromIntegral xs

instance Show DomainElement where
  show (DE xs) = show xs

newtype IntRange = IR [(Int,Int)]
                deriving (Show, Eq, Ord)

toList :: IntRange -> [DomainElement]
toList (IR xs) = map DE $ mapM (\(x,y) -> [x..y]) xs

transList :: IntRange -> DomainElement -> [(DomainElement, DomainElement)]
transList (IR xs) (DE [u, v])
  | not.null.tail $ nub xs = error "non UxU"
  | otherwise = do
    w <- [i..j]
    return (domainElement [u, w], domainElement [w, v])
    where (i, j) = head xs
transList _ _ = error "non UxU"

isUTimesUDomain :: IntRange -> Bool
isUTimesUDomain (IR (x:xs)) = (null.tail.nub$xs) && all (==x) xs

cardinality :: IntRange -> Int
cardinality (IR xs) = product $ map rangeTupleSize xs

dimension :: IntRange -> Int
dimension (IR xs) = length xs

numberOfComponents :: IntRange -> Int
numberOfComponents (IR xs) = length xs

rangeTupleSize :: Num a => (a, a) -> a
rangeTupleSize (i, j) = j-i+1

domainElement :: [Int] -> DomainElement
domainElement = DE

constructIntRange :: Int -> Int -> IntRange
constructIntRange i j
  | i <= j = IR [(i,j-1)]
  | otherwise = error "constructIntRange i j : i has to be <= than j."

prettyPrint :: IntRange -> String -> IO ()
prettyPrint ir s = do
  putStrLn s
  forM_ (toList ir) $
    \x -> printf "Element domene: %s\n" (show x)
  printf "Kardinalitet domene je: %d\n" $ cardinality ir

-- | Assuming that domain elements are n-tuples always, no silly (2, (3, 4)).
-- | That thing is not easily typed.
combine :: IntRange -> IntRange -> IntRange
combine (IR xs) (IR ys) = IR (xs ++ ys)

-- | TODO inefficient
elementForIndex :: IntRange -> Int -> DomainElement
elementForIndex (IR xs) k = DE $ foldr f [] $ zip xs indices
  where sizes = map rangeTupleSize xs
        indices =
          let raw = scanr (\el (i, _) -> (i `div` el, i `mod` el)) (k, 0) sizes
          in map snd raw
        f ((i, _), m) ys = (i + m) : ys

indexOf :: IntRange -> DomainElement -> Int
indexOf (IR xs) (DE el) = sum $ zipWith3 f xs el sizes
  where sizes = scanr (*) 1 $ map rangeTupleSize (tail xs)
        -- ^ [(0,4),(0,2),(0,2)] -> [3,3,1]
        f (i,_) y sz = (y - i) * sz
        -- ^ [(0,4),(0,2),(0,2)] [4,1,1] [3,3,1]
        -- ^ (0,4) 4 3 -> (4-0) * 3 = 12
        -- ^ (0,2) 1 3 -> (1-0) * 3 = 3
        -- ^ (0,2) 0 1 -> (0-0) * 3 = 0
        -- ^ ---------------------------
        --                            15

matrixForm :: IntRange -> [[DomainElement]]
matrixForm (IR [(a, b), (c, d)]) = do
  x <- [a..b]
  return $ map (\y -> domainElement [x,y]) [c..d]
matrixForm _ = error "not binary"

merge :: IntRange -> IntRange -> IntRange
merge (IR [(a, b), (c, d)]) (IR [(e, f), (h, g)])
  | not $ c == e && d == f = error "not mergable"
  | otherwise = IR [(a, b), (h, g)]
merge _ _ = error "not mergable"
