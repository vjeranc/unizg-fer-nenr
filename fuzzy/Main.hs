module Main where

import NENR.IntRange
import NENR.Fuzzy.Set hiding (prettyPrint)
import qualified NENR.Fuzzy.Set as F (prettyPrint)
import NENR.Fuzzy.Operations
import qualified NENR.Fuzzy.Relations as Relations

import Text.Printf
import Control.Monad (forM_)

task1main1 :: IO ()
task1main1 = do
  let d1 = constructIntRange 0 5
      d2 = constructIntRange 0 3
      d3 = combine d1 d2
  prettyPrint d1 "Elementi domene d1:"
  prettyPrint d2 "Elementi domene d2:"
  prettyPrint d3 "Elementi domene d3:"

  print $ elementForIndex d3 0
  print $ elementForIndex d3 5
  print $ elementForIndex d3 14

  print $ indexOf d3 (domainElement [4,1])

task1main2 :: IO ()
task1main2 = do
  let d    = constructIntRange 0 11
      set1 = constructFuzzySet d [(domainElement [0], 1.0)
                                 ,(domainElement [1], 0.8)
                                 ,(domainElement [2], 0.6)
                                 ,(domainElement [3], 0.4)
                                 ,(domainElement [4], 0.2)
                                 ]
  F.prettyPrint set1 "Set1:"

  let d2 = constructIntRange (-5) 6
      idx = indexOf d2
      set2 =
        constructCalculatedFuzzySet d2 $
        lambdaFunction
          (fromIntegral . idx $ domainElement [-4])
          (fromIntegral . idx $ domainElement [0])
          (fromIntegral . idx $ domainElement [4])
      set3 =
        constructCalculatedFuzzySet d2 $
          gammaFunction
            (fromIntegral . idx $ domainElement [-4])
            (fromIntegral . idx $ domainElement [0])

  emptyLine

  F.prettyPrint set2 "Set2:"

  emptyLine

  F.prettyPrint set3 "Set3:"

task1main3 :: IO ()
task1main3 = do
  let d    = constructIntRange 0 11
      set1 = constructFuzzySet d [(domainElement [0], 1.0)
                                 ,(domainElement [1], 0.8)
                                 ,(domainElement [2], 0.6)
                                 ,(domainElement [3], 0.4)
                                 ,(domainElement [4], 0.2)
                                 ]
  F.prettyPrint set1 "Set1:"

  emptyLine

  let notSet1 = applyUnary set1 zadehNot

  F.prettyPrint notSet1 "notSet1:"

  emptyLine

  let union = applyBinary set1 notSet1 zadehOr

  F.prettyPrint union "Set1 union notSet1:"

  emptyLine

  let hinters = applyBinary set1 notSet1 (hamacherTNorm 1.0)

  F.prettyPrint hinters "Set1 intersection with notSet1 using parameterised Hamacher T norm with parameter 1.0:"

task2main1 = do
  let u = constructIntRange 1 6
      u2= combine u u
      r1 = constructFuzzySet u2 [(domainElement [1,1], 1.0),
                                 (domainElement [2,2], 1.0),
                                 (domainElement [3,3], 1.0),
                                 (domainElement [4,4], 1.0),
                                 (domainElement [5,5], 1.0),
                                 (domainElement [3,1], 0.5),
                                 (domainElement [1,3], 0.5)]
      r2 = constructFuzzySet u2 [(domainElement [1,1], 1),
                                 (domainElement [2,2], 1),
                                 (domainElement [3,3], 1),
                                 (domainElement [4,4], 1),
                                 (domainElement [5,5], 1),
                                 (domainElement [3,1], 0.5),
                                 (domainElement [1,3], 0.1)]
      r3 = constructFuzzySet u2 [(domainElement [1,1], 1),
                                 (domainElement [2,2], 1),
                                 (domainElement [3,3], 0.3),
                                 (domainElement [4,4], 1),
                                 (domainElement [5,5], 1),
                                 (domainElement [1,2], 0.6),
                                 (domainElement [2,1], 0.6),
                                 (domainElement [2,3], 0.7),
                                 (domainElement [3,2], 0.7),
                                 (domainElement [3,1], 0.5),
                                 (domainElement [1,3], 0.5)]
      r4 = constructFuzzySet u2 [(domainElement [1,1], 1),
                                 (domainElement [2,2], 1),
                                 (domainElement [3,3], 1),
                                 (domainElement [4,4], 1),
                                 (domainElement [5,5], 1),
                                 (domainElement [1,2], 0.4),
                                 (domainElement [2,1], 0.4),
                                 (domainElement [2,3], 0.5),
                                 (domainElement [3,2], 0.5),
                                 (domainElement [1,3], 0.4),
                                 (domainElement [3,1], 0.4)]
  let test1 = Relations.isUTimesURelation r1

  printf "r1 je definiran nad UxU? %s\n" (show test1)

  let test2 = Relations.isSymmetric r1

  printf "r1 je simetrična? %s\n" (show test2)

  let test3 = Relations.isSymmetric r2
      test4 = Relations.isReflexive r1
      test5 = Relations.isReflexive r3
      test6 = Relations.isMaxMinTransitive r3
      test7 = Relations.isMaxMinTransitive r4

  printf "r2 je simetrična? %s\n" (show test3)
  printf "r1 je refleksivna? %s\n" (show test4)
  printf "r3 je refleksivna? %s\n" (show test5)
  printf "r3 je max-min tranzitivna? %s\n" (show test6)
  printf "r4 je max-min tranzitivna? %s\n" (show test7)

task2main2 = do
  let u1 = constructIntRange 1 5
      u2 = constructIntRange 1 4
      u3 = constructIntRange 1 5

  let r1 = constructFuzzySet (combine u1 u2)
            [(domainElement [1,1], 0.3),
             (domainElement [1,2], 1),
             (domainElement [3,3], 0.5),
             (domainElement [4,3], 0.5)]

  let r2 = constructFuzzySet (combine u2 u3)
            [(domainElement [1,1], 1),
             (domainElement [2,1], 0.5),
             (domainElement [2,2], 0.7),
             (domainElement [3,3], 1),
             (domainElement [3,4], 0.4)]

  let r1r2 = Relations.compositionOfBinaryRelations r1 r2

  F.prettyPrint r1r2 "Kompozicija:"

task2main3 = do
  let u = constructIntRange 1 5

  let r = constructFuzzySet (combine u u)
            [(domainElement [1,1], 1),
             (domainElement [2,2], 1),
             (domainElement [3,3], 1),
             (domainElement [4,4], 1),
             (domainElement [1,2], 0.3),
             (domainElement [2,1], 0.3),
             (domainElement [2,3], 0.5),
             (domainElement [3,2], 0.5),
             (domainElement [3,4], 0.2),
             (domainElement [4,3], 0.2)]

  printf "Početna relacija je neizrazita relacija ekvivalencije? %s\n"
          (show $ Relations.isFuzzyEquivalence r)
  emptyLine

  let c = tail . scanl1 Relations.compositionOfBinaryRelations $ repeat r

  forM_ (zip [1..3] c) $ \(i, r2) -> do
    printf "Broj odrađenih kompozicija: %d." (i :: Int)
    F.prettyPrint r2 " Relacija je:"
    printf "Ova relacija je neizrazita relacija ekvivalencije? %s"
            (show $ Relations.isFuzzyEquivalence r2)
    emptyLine


emptyLine :: IO ()
emptyLine = putStrLn ""

separate :: IO ()
separate = do
  emptyLine
  putStrLn "---------------------------------------------"
  emptyLine

main :: IO ()
main = do
  task1main1
  separate
  task1main2
  separate
  task1main3
  separate
  task2main1
  separate
  task2main2
  separate
  task2main3
