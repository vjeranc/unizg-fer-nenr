module Main where

import Text.Printf
import NENR.Fuzzy.Set
import NENR.IntRange
import NENR.Fuzzy.Operations
import NENR.Fuzzy.Rules
import Control.Monad (unless)
import System.IO

data Sizes = L | D | LK | DK | V | S deriving (Eq, Ord, Show)

mainDomain = constructIntRange 0 1301
-- distances
distDomain = constructIntRange 0 1301
veryClose = constructLSet distDomain 50 70
close = constructLambdaSet distDomain 50 75 100
moderateClose = constructLambdaSet distDomain 75 100 125
far = constructGammaSet distDomain 100 155
-- speed
speedDomain = constructIntRange 0 300
verySlow = constructLSet speedDomain 18 35
slow = constructLambdaSet speedDomain 20 55 90
optimal = constructLambdaSet speedDomain 58 88 110
fast = constructLambdaSet speedDomain 50 60 70
veryFast = constructGammaSet speedDomain 60 80

-- status of direction
dirDomain = constructIntRange 0 2
rightDirection = constructFuzzySet dirDomain [(domainElement [0], 0), (domainElement [1], 1)]
wrongDirection = applyUnary rightDirection zadehNot

-- Acceleration
accRange = constructIntRange (-300) (301)
slowDown = constructLambdaSet accRange (-11) (-6) (-2)
quickSlowDown = constructLambdaSet accRange (-20) (-15) (-10)
speedUp = constructLambdaSet accRange 2 8 13
quickSpeedUp = constructLambdaSet accRange 10 15 20
keepSpeed = constructLambdaSet accRange (-1) 0 1

-- direction
angleDomain = constructIntRange (-90) 91
turnSharpRight = constructLSet angleDomain (-80) (-75)
sharpishRight = constructLambdaSet angleDomain (-80) (-70) (-60)
turnRight = constructLambdaSet angleDomain (-70) (-55) (-40)
turnRightish = constructLambdaSet angleDomain (-45) (-15) (-0)
straight = constructLambdaSet angleDomain (-5) (-0) (5)
turnSharpLeft = constructGammaSet angleDomain 75 80
sharpishLeft = constructLambdaSet angleDomain 60 70 80
turnLeft = constructLambdaSet angleDomain 40 55 70
turnLeftish = constructLambdaSet angleDomain 10 15 45

--rules
--
rulesForTurn =
  [
  createFuzzyRule [(L, applyBinary veryClose close zadehOr),(LK, far)] turnLeft
  ,createFuzzyRule [(D, applyBinary veryClose close zadehOr),(DK, far)] turnRight
  -- izravnaj
  ,createFuzzyRule [(L, veryClose), (LK, applyBinary veryClose close zadehOr), (D, applyUnary veryClose zadehNot)] turnSharpRight
  ,createFuzzyRule [(L, close), (LK, moderateClose)] turnRightish
  ,createFuzzyRule [(D, veryClose), (DK, applyBinary veryClose close zadehOr), (L, applyUnary veryClose zadehNot)] turnSharpLeft
  ,createFuzzyRule [(D, close), (DK, moderateClose)] turnLeftish
  -- očuvaj smjer
  ,createFuzzyRule [(DK, veryClose), (LK, veryClose)] straight
  ,createFuzzyRule [(DK, close), (LK, close)] straight
  ,createFuzzyRule [(DK, moderateClose), (LK, moderateClose)] straight
  -- oštra skretanja
  ,createFuzzyRule [(LK, veryClose)] turnSharpRight
  ,createFuzzyRule [(DK, veryClose)] turnSharpLeft
  -- okreni se
  ,createFuzzyRule [(S, wrongDirection), (DK, applyBinary far close zadehOr)] turnSharpRight
  ,createFuzzyRule [(S, wrongDirection), (LK, applyBinary far close zadehOr)] turnSharpLeft
  ]
-- rules for speed
rulesForSpeed =
  [
  -- smjer ravno
  createFuzzyRule [(DK, moderateClose), (D, moderateClose), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] quickSpeedUp
  ,createFuzzyRule [(LK, moderateClose), (L, moderateClose), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] quickSpeedUp
  -- smjer ravno, ali blizu
  ,createFuzzyRule [(DK, close), (D, close), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] speedUp
  ,createFuzzyRule [(LK, close), (L, close), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] speedUp
  -- centar
  ,createFuzzyRule [(DK, far), (LK, far), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] quickSpeedUp
  ,createFuzzyRule [(DK, moderateClose), (LK, moderateClose), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] speedUp
  ,createFuzzyRule [(DK, close), (LK, close), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] speedUp
  ,createFuzzyRule [(DK, veryClose), (LK, veryClose), (S, rightDirection), (V, applyBinary verySlow slow zadehOr)] speedUp
  -- održavaj brzinu
  ,createFuzzyRule [(V, optimal)] keepSpeed
  -- wrong direction
  ,createFuzzyRule [(S, wrongDirection)] slowDown
  ]

main :: IO ()
main = do
  ls <- fmap ((map words).lines) $ getContents
  loop ls

loop :: [[String]] -> IO ()
loop [["Kraj"]] = return ()
loop (line:xs) = do
  let vals = zip [L, D, LK, DK, V, S] $ map (domainElement . (\x -> [x]) . read) $ line
  let acc = getComponent 0 $ inferFromRules vals rulesForSpeed :: Int
  let dir = getComponent 0 $ inferFromRules vals rulesForTurn  :: Int
  hPrintf stdout "%d %d\n" acc dir
  hFlush stdout
  hPrintf stderr "%d %d\n" acc dir
  hFlush stderr
  loop xs
