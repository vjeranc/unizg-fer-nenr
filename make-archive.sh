#!/usr/bin/env bash
git archive --format=zip --prefix=$1/ -9 HEAD > $1.zip
