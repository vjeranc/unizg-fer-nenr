# Fuzzy, Evolutionary and Neuro-computing

## Homeworks

Repository contains solution to homework tasks but solutions aren't separated
according to homeworks. Check the commit history to find the right separation.

Homeworks are tagged (using git) with the name of `tasks/*.pdf`.

To look at the code from a given homework (ex. zad1.pdf) write:

`git checkout -b homework1 zad1`

This will put all of the commits that lead to finishing of tasks in `zad1.pdf`
to branch `homework1`.

## Requirements

* GHC compiler
* Cabal
* stack (optional)
