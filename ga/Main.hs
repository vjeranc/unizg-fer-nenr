{-# LANGUAGE RecordWildCards #-}
module Main where

import GA.GeneticAlg (func)
import System.Environment
import System.IO
import System.Random

f [x,y,b0,b1,b2,b3,b4] = (b0 + b1 * x) + b2 * cos (x*(b3 + y)) / (1 + exp ((x - b4)*(x - b4)))

data G = G {
    fitness :: [Double] -> Double
  , v :: [Double]
}

calcFitness :: [[Double]] -> [Double] -> Double
calcFitness xs ys@[b0,b1,b2,b3,b4] = sum $ map (`sqrerr` ys) xs

sqrerr [x,y,z] [b0,b1,b2,b3,b4] = (z - f [x,y,b0,b1,b2,b3,b4]) ** 2

crossover x y =
  let v1 = v x
      v2 = v y
  in x {v=map (/2) $ zipWith (+) v1 v2}

mutateData [b0,b1,b2,b3,b4] = map ((\x -> if x > 4.0 then x-4.0 else x) . (+0.01)) [b2,b0,b1,b4,b3]

mutate x@G{..} = x{v = mutateData v}

createPopulation n xs = foldr (\_ (pop, rs) -> (G{v=take 5 rs}:pop, drop 5 rs) ) ([], xs) [1..n]

main = do
  [_, dat, popSize, elimination, elitism] <- getArgs
  ls <- fmap (map (map read . words) . lines) $ readFile dat :: IO [[Double]]
  let fitnessFunc = calcFitness ls
  gen <- newStdGen
  let infinityList = randomRs (-4.0, 4.0) gen :: [Double]



  print 5
